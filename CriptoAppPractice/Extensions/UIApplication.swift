//
//  UIApplication.swift
//  CriptoAppPractice
//
//  Created by Shakhawat Hossain Shahin on 23/6/21.
//

import Foundation
import SwiftUI

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
