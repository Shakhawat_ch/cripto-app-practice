//
//  MarketDataModel.swift
//  CriptoAppPractice
//
//  Created by Shakhawat Hossain Shahin on 6/7/21.
//

import Foundation

// JSON Data
/*
 URL: https://api.coingecko.com/api/v3/global

 JSON Response:
 {
 "data": {
   "active_cryptocurrencies": 8343,
   "upcoming_icos": 0,
   "ongoing_icos": 50,
   "ended_icos": 3375,
   "markets": 625,
   "total_market_cap": {
     "btc": 43169705.437766105,
     "eth": 651223618.4735638,
     "ltc": 10549883182.539915,
     "bch": 2869964665.157239,
     "bnb": 4927863930.280682,
     "eos": 374422260899.8221,
     "xrp": 2191209786424.4429,
     "xlm": 5649077455581.13,
     "link": 79216863923.40222,
     "dot": 95600875424.00044,
     "yfi": 45482077.459175706,
     "usd": 1480513554012.673,
     "aed": 5438222386599.355,
     "ars": 141822242674114.34,
     "aud": 1965775559557.1926,
     "bdt": 125270509761993.73,
     "bhd": 558110674969.7109,
     "bmd": 1480513554012.673,
     "brl": 7489744849664.286,
     "cad": 1826220871442.402,
     "chf": 1364408720079.8936,
     "clp": 1087363179744609.1,
     "cny": 9563525353500.26,
     "czk": 31878540727625.863,
     "dkk": 9274296667128.99,
     "eur": 1247168332251.182,
     "gbp": 1068905617266.7332,
     "hkd": 11500999415958.96,
     "huf": 438132145426479.06,
     "idr": 21413242542422492,
     "ils": 4830989752421.069,
     "inr": 110060632906984.3,
     "jpy": 164134914395283.97,
     "krw": 1672728245277130,
     "kwd": 445804838816.5255,
     "lkr": 294555694060189.2,
     "mmk": 2430268808446990.5,
     "mxn": 29309535842540.438,
     "myr": 6153458484542.862,
     "ngn": 607928475548683.5,
     "nok": 12704720597454.096,
     "nzd": 2107493277974.3916,
     "php": 72835902956817.17,
     "pkr": 232396180002071.25,
     "pln": 5621727600078.549,
     "rub": 108508022784432.08,
     "sar": 5553213874339.511,
     "sek": 12656987359959.145,
     "sgd": 1991616443128.931,
     "thb": 47554095354886.88,
     "try": 12867143297924.16,
     "twd": 41307069894244.164,
     "uah": 40486637386233.805,
     "vef": 148243822163.28912,
     "vnd": 33925129402960270,
     "zar": 21095008543536.32,
     "xdr": 1037447665271.069,
     "xag": 55661580043.42264,
     "xau": 825800850.1571895,
     "bits": 43169705437766.1,
     "sats": 4316970543776610.5
   },
   "total_volume": {
     "btc": 2534647.307290688,
     "eth": 38235660.26846467,
     "ltc": 619421252.2345122,
     "bch": 168505856.9845234,
     "bnb": 289332459.3003564,
     "eos": 21983665761.804993,
     "xrp": 128653738276.63339,
     "xlm": 331677476514.3307,
     "link": 4651104490.970425,
     "dot": 5613068215.566083,
     "yfi": 2670414.9123295858,
     "usd": 86926228822.55582,
     "aed": 319297423711.0123,
     "ars": 8326889467107.559,
     "aud": 115417691138.80975,
     "bdt": 7355078220510.549,
     "bhd": 32768667405.46766,
     "bmd": 86926228822.55582,
     "brl": 439749621244.53723,
     "cad": 107223937883.7667,
     "chf": 80109300105.83348,
     "clp": 63842968758726.2,
     "cny": 561508667702.1814,
     "czk": 1871702773884.2644,
     "dkk": 544527020418.0924,
     "eur": 73225698971.60399,
     "gbp": 62759259463.9954,
     "hkd": 675264677050.8201,
     "huf": 25724300209637.535,
     "idr": 1257247808390933,
     "ils": 283644630959.4417,
     "inr": 6462052126775.692,
     "jpy": 9636945969069.416,
     "krw": 98211838596695.1,
     "kwd": 26174791391.903862,
     "lkr": 17294414896416.125,
     "mmk": 142689745710747.38,
     "mxn": 1720867338516.6204,
     "myr": 361291484855.1881,
     "ngn": 35693648079117.85,
     "nok": 745939438911.3978,
     "nzd": 123738443614.16231,
     "php": 4276455524342.7124,
     "pkr": 13644808225899.562,
     "pln": 330071668994.88074,
     "rub": 6370892851388.178,
     "sar": 326048983903.65967,
     "sek": 743136850367.9281,
     "sgd": 116934901536.67871,
     "thb": 2792070469780.4824,
     "try": 755475854696.8337,
     "twd": 2425285334189.949,
     "uah": 2377114816783.013,
     "vef": 8703923292.002523,
     "vnd": 1991865291151058.2,
     "zar": 1238563155804.4565,
     "xdr": 60912250953.973564,
     "xag": 3268089799.222565,
     "xau": 48485711.91264523,
     "bits": 2534647307290.688,
     "sats": 253464730729068.8
   },
   "market_cap_percentage": {
     "btc": 43.42932575027124,
     "eth": 17.919285811621656,
     "usdt": 4.2332426915336345,
     "bnb": 3.1356879367447883,
     "ada": 3.0806638670644215,
     "doge": 2.130620013642547,
     "xrp": 2.1079335773730246,
     "usdc": 1.72553039044979,
     "dot": 1.0563354286186,
     "busd": 0.7228533440653417
   },
   "market_cap_change_percentage_24h_usd": -2.6547816175919743,
   "updated_at": 1625473642
 }
}
 */

struct GlobalData : Codable {
    let data: MarketDataModel?
}

// MARK: - DataClass
struct MarketDataModel: Codable {
     let totalMarketCap, totalVolume, marketCapPercentage: [String: Double]
    let marketCapChangePercentage24HUsd: Double

    enum CodingKeys: String, CodingKey {
        case totalMarketCap = "total_market_cap"
        case totalVolume = "total_volume"
        case marketCapPercentage = "market_cap_percentage"
        case marketCapChangePercentage24HUsd = "market_cap_change_percentage_24h_usd"
    }
    
    var marketCap: String {
        if let item = totalMarketCap.first(where: { $0.key == "usd" }) {
            return "$" + item.value.formattedWithAbbreviations()
        }
        return ""
    }
    
    var volume: String {
        if let item = totalVolume.first(where: { $0.key == "usd" }) {
            return "$" + item.value.formattedWithAbbreviations()
        }
        return ""
    }
    
    var btcDominance: String {
        if let item = marketCapPercentage.first(where: { $0.key == "btc" }) {
            return item.value.asPercentString()
        }
        return ""
    }
}
