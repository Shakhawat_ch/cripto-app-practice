//
//  CoinModel.swift
//  CriptoAppPractice
//
//  Created by Shakhawat Hossain Shahin on 22/6/21.
//

/*
 
 https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=250&page=1&sparkline=true&price_change_percentage=24h
 
{
    "id": "bitcoin",
    "symbol": "btc",
    "name": "Bitcoin",
    "image": "https://assets.coingecko.com/coins/images/1/large/bitcoin.png?1547033579",
    "current_price": 32862,
    "market_cap": 618798858884,
    "market_cap_rank": 1,
    "fully_diluted_valuation": 693412078799,
    "total_volume": 55315832472,
    "high_24h": 34656,
    "low_24h": 31252,
    "price_change_24h": -1251.980749852941,
    "price_change_percentage_24h": -3.66995,
    "market_cap_change_24h": -18551503338.316895,
    "market_cap_change_percentage_24h": -2.91072,
    "circulating_supply": 18740337,
    "total_supply": 21000000,
    "max_supply": 21000000,
    "ath": 64805,
    "ath_change_percentage": -49.03155,
    "ath_date": "2021-04-14T11:54:46.763Z",
    "atl": 67.81,
    "atl_change_percentage": 48610.28993,
    "atl_date": "2013-07-06T00:00:00.000Z",
    "roi": null,
    "last_updated": "2021-06-22T05:22:26.317Z",
    "sparkline_in_7d": {
      "price": [
        40624.51305316154,
        40418.25395566633,
        40147.17116009384,
        40273.584027861536,
        40517.81222640135,
        40666.80142029716,
        40596.15514362545,
        40420.16615306266,
        40421.174985135265,
        39928.7414517808,
        40143.760755532414,
        40068.48099549366,
        39805.65495364918,
        40324.420891984315,
        40348.8259814203,
        40155.75905450585,
        40127.757745330084,
        40405.39921867471,
        40649.232534819945,
        40517.03786195838,
        40013.22802464948,
        40049.34259157463,
        40044.37875999401,
        40107.39933367642,
        40378.20254971114,
        40021.364141242324,
        40158.906553938796,
        40095.87553625936,
        40179.57321926227,
        40517.290736659976,
        40396.16195872671,
        40492.9495854452,
        40281.77921786202,
        40164.14444793276,
        40116.04623526945,
        39798.37085269288,
        39181.78602676818,
        39216.292678244514,
        39263.26629365373,
        39022.43809120344,
        38871.078022236135,
        38411.14055781992,
        39037.61941140664,
        38757.28003702707,
        38783.967912117376,
        38763.933741823246,
        38841.8167574429,
        38596.6615862117,
        38321.454656574206,
        38675.60923094213,
        38655.77260291506,
        38842.53631958052,
        38994.464771096944,
        38849.277980568164,
        38868.38203193853,
        39296.36205399863,
        39377.29036080241,
        39323.09445238778,
        39502.727671374785,
        39362.79990104263,
        39298.72400853465,
        38900.23329432912,
        38882.66788350975,
        38956.04480558058,
        39151.98188477147,
        38474.000169829844,
        37851.79469864162,
        37815.612886262585,
        37762.79066212605,
        37730.60938136989,
        37820.55937632195,
        37832.14634017445,
        38193.619141943964,
        37997.10316607907,
        37761.09845014525,
        38071.474240279,
        38046.1581054594,
        38158.221528242044,
        37789.554210311675,
        37604.00623713204,
        37821.61919649231,
        38107.46498065815,
        37643.509075826005,
        38076.098904483435,
        37881.19511620497,
        37382.793846043445,
        37213.78322341964,
        36741.81794923324,
        36420.6544406319,
        36627.3654074224,
        36367.79734531611,
        35335.928275028484,
        35462.57402457577,
        35391.645593747664,
        35685.26027635894,
        35515.72817330246,
        35773.76501811242,
        35937.859059843744,
        36174.50365110562,
        35535.55336551567,
        35108.23680186193,
        35185.38887414794,
        35626.93528217788,
        35855.85745121444,
        35881.518287153995,
        36028.37953770993,
        35809.168868345456,
        35941.634121004994,
        36070.82207093339,
        35751.41347229755,
        35909.260459861995,
        36253.12497593591,
        36087.228242105666,
        35992.40994021323,
        35840.416977011046,
        35559.29602986992,
        36008.17210735436,
        36297.33756251337,
        36057.056962955205,
        35776.05642559253,
        35681.02611983412,
        35547.31045597305,
        35757.493634707396,
        35597.87291229503,
        35713.12645452607,
        35920.66675094305,
        35853.41748090548,
        35558.224465779356,
        35658.48192642483,
        35264.20880646201,
        35051.04125022762,
        34170.38851156312,
        33928.23940747932,
        33933.98893777933,
        34102.61534965846,
        34146.426674305425,
        34665.44940756869,
        34609.60511979082,
        34546.434799136434,
        35031.84839298748,
        35831.241670171454,
        35902.41954365804,
        35667.04119113474,
        35793.10276191479,
        35787.07740523232,
        35607.57981445491,
        35417.56238398955,
        35099.42070534751,
        34803.40040556135,
        34010.726124858724,
        34550.10678155774,
        32986.896075732555,
        32961.91862671857,
        32920.34784361721,
        33322.83941261687,
        32474.935058224884,
        32648.370710220348,
        32255.47063277272,
        33116.428260420005,
        33075.18739307036,
        32662.72929435887,
        32052.718425838633,
        32343.724922412406,
        32886.124626079036,
        32448.67924669863,
        32716.772600032535,
        31586.87699370606,
        31512.945472893447
      ]
    },
    "price_change_percentage_24h_in_currency": -3.669948230693903
  }
 
*/



import Foundation
import SwiftUI

struct CoinModel: Identifiable, Codable {
    let id, symbol, name: String
    let image: String
    let currentPrice: Double
    let marketCap, marketCapRank, fullyDilutedValuation: Double?
    let totalVolume, high24H, low24H: Double?
    let priceChange24H, priceChangePercentage24H, marketCapChange24H, marketCapChangePercentage24H: Double?
    let circulatingSupply, totalSupply, maxSupply, ath: Double?
    let athChangePercentage: Double?
    let athDate: String?
    let atl, atlChangePercentage: Double?
    let atlDate, lastUpdated: String?
    let sparklineIn7D: SparklineIn7D?
    let priceChangePercentage24HInCurrency: Double?
    let currentHoldings: Double?

    enum CodingKeys: String, CodingKey {
        case id, symbol, name, image
        case currentPrice = "current_price"
        case marketCap = "market_cap"
        case marketCapRank = "market_cap_rank"
        case fullyDilutedValuation = "fully_diluted_valuation"
        case totalVolume = "total_volume"
        case high24H = "high_24h"
        case low24H = "low_24h"
        case priceChange24H = "price_change_24h"
        case priceChangePercentage24H = "price_change_percentage_24h"
        case marketCapChange24H = "market_cap_change_24h"
        case marketCapChangePercentage24H = "market_cap_change_percentage_24h"
        case circulatingSupply = "circulating_supply"
        case totalSupply = "total_supply"
        case maxSupply = "max_supply"
        case ath
        case athChangePercentage = "ath_change_percentage"
        case athDate = "ath_date"
        case atl
        case atlChangePercentage = "atl_change_percentage"
        case atlDate = "atl_date"
        case lastUpdated = "last_updated"
        case sparklineIn7D = "sparkline_in_7d"
        case priceChangePercentage24HInCurrency = "price_change_percentage_24h_in_currency"
        case currentHoldings
    }
    
    func updateHoldings(amount: Double) -> CoinModel {
        return CoinModel(id: id, symbol: symbol, name: name, image: image, currentPrice: currentPrice, marketCap: marketCap, marketCapRank: marketCapRank, fullyDilutedValuation: fullyDilutedValuation, totalVolume: totalVolume, high24H: high24H, low24H: low24H, priceChange24H: priceChange24H, priceChangePercentage24H: priceChangePercentage24H, marketCapChange24H: marketCapChange24H, marketCapChangePercentage24H: marketCapChangePercentage24H, circulatingSupply: circulatingSupply, totalSupply: totalSupply, maxSupply: maxSupply, ath: ath, athChangePercentage: athChangePercentage, athDate: athDate, atl: atl, atlChangePercentage: atlChangePercentage, atlDate: atlDate, lastUpdated: lastUpdated, sparklineIn7D: sparklineIn7D, priceChangePercentage24HInCurrency: priceChangePercentage24HInCurrency, currentHoldings: amount)
    }
    
    var currentHoldingsValue: Double {
        return (currentHoldings ?? 0) * currentPrice
    }
    
    var rank: Int {
        return Int(marketCapRank ?? 0)
    }
}

// MARK: - SparklineIn7D
struct SparklineIn7D: Codable {
    let price: [Double]?
}
