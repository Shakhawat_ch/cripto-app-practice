//
//  XmarkButtonView.swift
//  CriptoAppPractice
//
//  Created by Shakhawat Hossain Shahin on 7/7/21.
//

import SwiftUI

struct XmarkButtonView: View {
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        Button(action: {
            self.presentationMode.wrappedValue.dismiss()
        }, label: {
            Image(systemName: "xmark")
                .font(.headline)
        })
    }
}

struct XmarkButtonView_Previews: PreviewProvider {
    static var previews: some View {
        XmarkButtonView()
    }
}
